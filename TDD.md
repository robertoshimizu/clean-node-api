## Testing Driven Development

This is a the lessons learned regarding `TDD` from the course [NodeJs, Typescript, TDD, DDD, Clean Architecture e SOLID](https://www.udemy.com/course/tdd-com-mango/) from [Rodrigo Manguinho](https://www.linkedin.com/in/rmanguinho).


### What is TDD?

### Concepts

The System Under Test (SUT) from a Unit Testing perspective represents all of the actors (i.e one or more classes) in a test that are not `mocks` or `stubs`. 

Let\`s pick one: the `SignUpController` class. This controller has the responsibility to validate the data coming from the http POST request. If the data is valid (according to the business logic), then it creates a new account, otherwise, it throws a series of errors, depending on the nature ot the error. 

```typescript
describe('SignUp Controller', () => {
  test('Should return 400 if no email is provided', async () => {
    const { sut } = makeSut()
    const httpRequest = {
      body: {
        name: 'any_name',
        password: 'any_password',
        passwordConfirmation: 'any_password'
      }
    }
    const httpResponse = await sut.handle(httpRequest)
    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new MissingParamError('email'))
  })
})

describe('SignUp Controller', () => {
  test('Should return 400 if password confirmation fails', async () => {
    const { sut } = makeSut()
    const httpRequest = {
      body: {
        name: 'any_name',
        email: 'any@email.com',
        password: 'any_password',
        passwordConfirmation: 'different_password'
      }
    }
    const httpResponse = await sut.handle(httpRequest)
    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new InvalidParamError('passwordConfirmation'))
  })
})
```


This class also depends on another class called EmailValidator. In this architecture, `AddAccount` and `EmailValidator` are injected in the `SignUpController` constructor. For unit test purposes, you make these two classes a `stub`, and then `mock` the response. This technique is called `Test Doubles` (idea from the cinema double). According Gerard Meszaros, the creator of the term `Tests Doubles`, these can be divided into 5 categories: Mocks, Stubs, Fakes, Spies and Dummies.

```typescript
// makeSut implements the interface SutTypes
const makeSut = (): SutTypes => {
  const emailValidatorStub = makeEmailValidator()
  // New AddAccount Stub
  const addAccountStub = makeAddAccount()
  const sut = new SignUpController(emailValidatorStub, addAccountStub)
  return {
    sut,
    emailValidatorStub,
    addAccountStub
  }
}
```
Note the use of `interface` in typescript (do not confuse with Interface - OOP). In typescript, `interface` is a structure that defines the contract in your application. It defines the syntax for classes to follow. Classes that are derived from an `interface` must follow the structure provided by their interface. The TypeScript compiler does not convert `interface` to JavaScript. 
In this example, the function `makeSut()` returns an `interface` of the type `SutTypes`, which by definition, contains the objects of the classes `SignUpController`, `EmailValidator` and `AddAccount`.

```typescript
// Interface as Type
interface SutTypes{
  sut: SignUpController
  emailValidatorStub: EmailValidator
  addAccountStub: AddAccount
}
```

`Stubs` provide canned answers (predetermined responses to common questions) to calls made during the test, usually not responding at all to anything outside what's programmed in for the test. `Stubs` may also record information about calls, such as an email gateway stub that remembers the messages it 'sent', or maybe only how many messages it 'sent'.

```typescript
// Factory do Mock EmailValidator
const makeEmailValidator = (): EmailValidator => {
  class EmailValidatorStub implements EmailValidator {
    // Override isValid from the EmailValidator protocol
    isValid (email: string): boolean {
      return true
    }
  }
  return new EmailValidatorStub()
}
```
```typescript
// Factory do Mock AddAccount
const makeAddAccount = (): AddAccount => {
  class AddAccountStub implements AddAccount {
    // Override addNewAccount from the AddAccount protocol
    async addNewAccount (account: AddAccountModel): Promise<AccountModel> {
      // AddAccountModel is what you send and AccountModel is what you get from the server/domain
      const fakeAccount = {
        uid: 'valid_uid',
        name: 'valid_name',
        email: 'valid@email.com',
        password: 'valid_password'
      }
      return await new Promise(resolve => resolve(fakeAccount))
    }
  }
  return new AddAccountStub()
}
```
`Mocks` are objects pre-programmed with expectations which form a specification of the calls they are expected to receive.

```typescript
describe('SignUp Controller', () => {
  test('Should return 400 if email is invalid', async () => {
    const { sut, emailValidatorStub } = makeSut()

    // Here is where the emailValidatorStub will receive the value only once to "false"
    // so the test may pass
    // Tá marretando false "somente desta vez"
    jest.spyOn(emailValidatorStub, 'isValid').mockReturnValueOnce(false)

    const httpRequest = {
      body: {
        name: 'any_name',
        email: 'any@email.com',
        password: 'any_password',
        passwordConfirmation: 'any_password'
      }
    }
    const httpResponse = await sut.handle(httpRequest)
    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new InvalidParamError('email'))
  })
})
```

`jest.spyOn` allows you to mock either the whole module or the individual functions of the module. At its most general usage, it can be used to track calls on a method. In the above example, jest mocked a *false* response from the method `isValid`.
In the example below, it only checks if the method were called, and with which parameter.

```typescript
describe('SignUp Controller', () => {
  test('Should call EmailValidator with correct email', async () => {
    const { sut, emailValidatorStub } = makeSut()

    const isValidSpy = jest.spyOn(emailValidatorStub, 'isValid')

    const httpRequest = {
      body: {
        name: 'any_name',
        email: 'any@email.com',
        password: 'any_password',
        passwordConfirmation: 'any_password'
      }
    }
    await sut.handle(httpRequest)
    expect(isValidSpy).toHaveBeenCalledWith('any@email.com')
  })
})
```
Below, the mock response is defined on the fly (in the test):
```typescript
describe('SignUp Controller', () => {
  test('Should return 500 if AddAccount throws an Exception', async () => {
    const { sut, addAccountStub } = makeSut()
    jest.spyOn(addAccountStub, 'addNewAccount').mockImplementationOnce(async () => {
      return await new Promise((resolve, reject) => reject(new Error()))
    })

    const httpRequest = {
      body: {
        name: 'any_name',
        email: 'any@email.com',
        password: 'any_password',
        passwordConfirmation: 'any_password'
      }
    }
    const httpResponse = await sut.handle(httpRequest)
    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError('ok'))
  })
})
```
And lastly, do not mock response in the case of success, since it would have to be the normal response of your sut.
```typescript
describe('SignUp Controller', () => {
  test('Should return 200 if AddAccount returns Success', async () => {
    const { sut } = makeSut()
    const httpRequest = {
      body: {
        name: 'valid_name',
        email: 'valid@email.com',
        password: 'valid_password',
        passwordConfirmation: 'valid_password'
      }
    }
    const httpResponse = await sut.handle(httpRequest)
    expect(httpResponse.statusCode).toBe(200)
    expect(httpResponse.body).toEqual({
      uid: 'valid_uid',
      name: 'valid_name',
      email: 'valid@email.com',
      password: 'valid_password'
    })
  })
})
```


In our example we want that the test fail only once, so we need to use `jest` to fabricate a false return so our test can fail.





Return to [Clean-Node-API project](./README.md).
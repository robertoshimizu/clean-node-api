export interface AccountModel{
  uid: string
  name: string
  email: string
  password: string
}

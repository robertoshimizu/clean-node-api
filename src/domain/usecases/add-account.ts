import { AccountModel } from '../models/account'

export interface AddAccount{
  addNewAccount: (account: AddAccountModel) => Promise<AccountModel>
}

export interface AddAccountModel{
  name: string
  email: string
  password: string
}

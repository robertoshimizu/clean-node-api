import { SignUpController } from './signup'
import { MissingParamError, InvalidParamError, ServerError } from '../../errors'
import { EmailValidator, AddAccount, AddAccountModel, AccountModel } from './signup-protocols'

// Factory pattern
// The System Under Test (SUT) from a Unit Testing perspective represents all of the actors
// (i.e one or more classes) in a test that are not mocks or stubs.
// In your example that would be the SignUpController.

// Stubs provide canned answers to calls made during the test, usually not responding at all
// to anything outside what's programmed in for the test. Stubs may also record information about calls,
// such as an email gateway stub that remembers the messages it 'sent', or maybe only how many messages
// it 'sent'.
// Mocks are objects pre-programmed with expectations which form a specification of the calls they
// are expected to receive.
// In our example we want that the test fail only once, so we need to use jest to fabricate a false return
// so our test can fail.

// Mock do EmailValidator
const makeEmailValidator = (): EmailValidator => {
  class EmailValidatorStub implements EmailValidator {
    // Override isValid from the EmailValidator protocol
    isValid (email: string): boolean {
      return true
    }
  }
  return new EmailValidatorStub()
}

// Deprecated
// const makeEmailValidatorWithError = (): EmailValidator => {
//   class EmailValidatorStub implements EmailValidator {
//     // Override isValid from the EmailValidator protocol
//     isValid (email: string): boolean {
//       // Simulate that in any case, the EmailValidator fails and throws an Error
//       // Therefore the test need to simulate a correct treatment
//       throw new Error()
//     }
//   }
//   return new EmailValidatorStub()
// }

// Interface as Type
interface SutTypes{
  sut: SignUpController
  emailValidatorStub: EmailValidator
  addAccountStub: AddAccount
}

// makeSut implements the interface SutTypes
const makeSut = (): SutTypes => {
  const emailValidatorStub = makeEmailValidator()
  // New AddAccount Stub
  const addAccountStub = makeAddAccount()
  const sut = new SignUpController(emailValidatorStub, addAccountStub)
  return {
    sut,
    emailValidatorStub,
    addAccountStub
  }
}

describe('SignUp Controller', () => {
  test('Should return 400 if no name is provided', async () => {
    const { sut } = makeSut()
    const httpRequest = {
      body: {
        email: 'any@email.com',
        password: 'any_password',
        passwordConfirmation: 'any_password'
      }
    }
    const httpResponse = await sut.handle(httpRequest)
    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new MissingParamError('name'))
  })
})

describe('SignUp Controller', () => {
  test('Should return 400 if no email is provided', async () => {
    const { sut } = makeSut()
    const httpRequest = {
      body: {
        name: 'any_name',
        password: 'any_password',
        passwordConfirmation: 'any_password'
      }
    }
    const httpResponse = await sut.handle(httpRequest)
    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new MissingParamError('email'))
  })
})

describe('SignUp Controller', () => {
  test('Should return 400 if no password is provided', async () => {
    const { sut } = makeSut()
    const httpRequest = {
      body: {
        name: 'any_name',
        email: 'any@email.com',
        passwordConfirmation: 'any_password'
      }
    }
    const httpResponse = await sut.handle(httpRequest)
    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new MissingParamError('password'))
  })
})

describe('SignUp Controller', () => {
  test('Should return 400 if no password confirmation is provided', async () => {
    const { sut } = makeSut()
    const httpRequest = {
      body: {
        name: 'any_name',
        email: 'any@email.com',
        password: 'any_password'
      }
    }
    const httpResponse = await sut.handle(httpRequest)
    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new MissingParamError('passwordConfirmation'))
  })
})

describe('SignUp Controller', () => {
  test('Should return 400 if password confirmation fails', async () => {
    const { sut } = makeSut()
    const httpRequest = {
      body: {
        name: 'any_name',
        email: 'any@email.com',
        password: 'any_password',
        passwordConfirmation: 'different_password'
      }
    }
    const httpResponse = await sut.handle(httpRequest)
    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new InvalidParamError('passwordConfirmation'))
  })
})

describe('SignUp Controller', () => {
  test('Should return 400 if email is invalid', async () => {
    const { sut, emailValidatorStub } = makeSut()

    // Here is where the emailValidatorStub will receive the value only once to "false"
    // so the test may pass
    // Tá marretando false "somente desta vez"
    jest.spyOn(emailValidatorStub, 'isValid').mockReturnValueOnce(false)

    const httpRequest = {
      body: {
        name: 'any_name',
        email: 'any@email.com',
        password: 'any_password',
        passwordConfirmation: 'any_password'
      }
    }
    const httpResponse = await sut.handle(httpRequest)
    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new InvalidParamError('email'))
  })
})

describe('SignUp Controller', () => {
  test('Should call EmailValidator with correct email', async () => {
    const { sut, emailValidatorStub } = makeSut()

    const isValidSpy = jest.spyOn(emailValidatorStub, 'isValid')

    const httpRequest = {
      body: {
        name: 'any_name',
        email: 'any@email.com',
        password: 'any_password',
        passwordConfirmation: 'any_password'
      }
    }
    await sut.handle(httpRequest)
    expect(isValidSpy).toHaveBeenCalledWith('any@email.com')
  })
})

// A forma abaixo foi deprecated por uma forma mais enxuta usando o jest

// describe('SignUp Controller', () => {
//   test('Should return 500 if EmailValidator throws an Exception', () => {
//     const emailValidatorStub = makeEmailValidatorWithError()
//     const sut = new SignUpController(emailValidatorStub)

//     const httpRequest = {
//       body: {
//         name: 'any_name',
//         email: 'any@email.com',
//         password: 'any_password',
//         passwordConfirmation: 'any_password'
//       }
//     }
//     const httpResponse = sut.handle(httpRequest)
//     expect(httpResponse.statusCode).toBe(500)
//     expect(httpResponse.body).toEqual(new ServerError())
//   })
// })

// Forma mais enxuta usando Jest

describe('SignUp Controller', () => {
  test('Should return 500 if EmailValidator throws an Exception', async () => {
    const { sut, emailValidatorStub } = makeSut()
    jest.spyOn(emailValidatorStub, 'isValid').mockImplementationOnce(() => {
      throw new Error()
    })

    const httpRequest = {
      body: {
        name: 'any_name',
        email: 'any@email.com',
        password: 'any_password',
        passwordConfirmation: 'any_password'
      }
    }
    const httpResponse = await sut.handle(httpRequest)
    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError('ok'))
  })
})

// Now it will start to test AddAccount Class

// Factory do Mock AddAccount
const makeAddAccount = (): AddAccount => {
  class AddAccountStub implements AddAccount {
    // Override addNewAccount from the AddAccount protocol
    async addNewAccount (account: AddAccountModel): Promise<AccountModel> {
      // AddAccountModel is what you send and AccountModel is what you get from the server/domain
      const fakeAccount = {
        uid: 'valid_uid',
        name: 'valid_name',
        email: 'valid@email.com',
        password: 'valid_password'
      }
      return await new Promise(resolve => resolve(fakeAccount))
    }
  }
  return new AddAccountStub()
}

describe('SignUp Controller', () => {
  test('Should call AddAccount with validated data', async () => {
    const { sut, addAccountStub } = makeSut()

    const addAccountSpy = jest.spyOn(addAccountStub, 'addNewAccount')

    const httpRequest = {
      body: {
        name: 'any_name',
        email: 'any@email.com',
        password: 'any_password',
        passwordConfirmation: 'any_password'
      }
    }
    await sut.handle(httpRequest)
    expect(addAccountSpy).toHaveBeenCalledWith({
      name: 'any_name',
      email: 'any@email.com',
      password: 'any_password'
    })
  })
})

describe('SignUp Controller', () => {
  test('Should return 500 if AddAccount throws an Exception', async () => {
    const { sut, addAccountStub } = makeSut()
    jest.spyOn(addAccountStub, 'addNewAccount').mockImplementationOnce(async () => {
      return await new Promise((resolve, reject) => reject(new Error()))
    })

    const httpRequest = {
      body: {
        name: 'any_name',
        email: 'any@email.com',
        password: 'any_password',
        passwordConfirmation: 'any_password'
      }
    }
    const httpResponse = await sut.handle(httpRequest)
    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError('ok'))
  })
})

describe('SignUp Controller', () => {
  test('Should return 200 if AddAccount returns Success', async () => {
    const { sut } = makeSut()
    const httpRequest = {
      body: {
        name: 'valid_name',
        email: 'valid@email.com',
        password: 'valid_password',
        passwordConfirmation: 'valid_password'
      }
    }
    const httpResponse = await sut.handle(httpRequest)
    expect(httpResponse.statusCode).toBe(200)
    expect(httpResponse.body).toEqual({
      uid: 'valid_uid',
      name: 'valid_name',
      email: 'valid@email.com',
      password: 'valid_password'
    })
  })
})

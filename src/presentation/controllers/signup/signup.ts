import { HttpRequest, HttpResponse, Controller, EmailValidator, AddAccount } from './signup-protocols'
import { MissingParamError, InvalidParamError } from '../../errors'
import { badRequest, serverError, ok } from '../../helpers/http-helper'

export class SignUpController implements Controller {
  private readonly emailValidator: EmailValidator
  private readonly addAcount: AddAccount

  // Injeção de Dependências ocorre no construtor da classe
  constructor (emailValidator: EmailValidator, addAccount: AddAccount) {
    this.emailValidator = emailValidator
    this.addAcount = addAccount
  }

  // override handle from Controller interface
  async handle (httpRequest: HttpRequest): Promise <HttpResponse> {
    try {
      // Check for missing params
      const requiredFields = ['name', 'email', 'password', 'passwordConfirmation']
      for (const field of requiredFields) {
        // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
        if (!httpRequest.body[field]) {
          return badRequest(new MissingParamError(field))
        }
      }

      // just a refactor to clean code
      const { name, email, password, passwordConfirmation } = httpRequest.body

      // Check password confirmation
      if (password !== passwordConfirmation) {
        return badRequest(new InvalidParamError('passwordConfirmation'))
      }
      // Check if email is valid
      const isValid = this.emailValidator.isValid(email)
      if (!isValid) {
        return badRequest(new InvalidParamError('email'))
      }
      // If program reaches this point, it means all validations passed
      // so it is is ready to add a new Account
      const account = await this.addAcount.addNewAccount({
        name,
        email,
        password
      })
      // Lembre-se que handle retorna um HttpResponse, definido na interface
      return ok(account)
    } catch (error) {
      // In case EmailValidator throws an error
      // one could try to debug what kind of error message the server would return
      // console.log(error)
      return serverError(error)
    }
  }
}

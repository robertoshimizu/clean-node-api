import { Authentication, AuthenticationModel } from '../../../domain/usecases/authentication'
import { InvalidParamError, MissingParamError } from '../../errors'
import { badRequest, ok, serverError, unauthorized } from '../../helpers/http-helper'
import { LoginController } from './login'
import { EmailValidator } from './login-protocols'

const makeEmailValidator = (): EmailValidator => {
  class EmailValidatorStub implements EmailValidator {
    isValid (email: string): boolean {
      return true
    }
  }
  return new EmailValidatorStub()
}

const makeAuthentication = (): Authentication => {
  class AuthenticationStub implements Authentication {
    async auth (authentication: AuthenticationModel): Promise<string> {
      return new Promise(resolve => resolve('any_token'))
    }
  }
  return new AuthenticationStub()
}

interface SutTypes {
  sut: LoginController
  emailValidatorStub: EmailValidator
  authenticationStub: Authentication
}

const makeSut = (): SutTypes => {
  const emailValidatorStub = makeEmailValidator()
  const authenticationStub = makeAuthentication()
  const sut = new LoginController(emailValidatorStub, authenticationStub)
  return {
    sut,
    emailValidatorStub,
    authenticationStub
  }
}

describe('Login Controller', () => {
  test('Should return 400 if no email is provided', async () => {
    const { sut } = makeSut()
    const httpRequest = {
      body: {
        password: 'any_password'
      }
    }
    const httpResponse = await sut.handle(httpRequest)
    expect(httpResponse).toEqual(badRequest(new MissingParamError('email')))
  })
})

test('Should return 400 if no password is provided', async () => {
  const { sut } = makeSut()
  const httpRequest = {
    body: {
      email: 'any_email@mail.com'
    }
  }
  const httpResponse = await sut.handle(httpRequest)
  expect(httpResponse).toEqual(badRequest(new MissingParamError('password')))
})

test('Should call EmailValidator with correct email', async () => {
  const { sut, emailValidatorStub } = makeSut()
  const isValidSpy = jest.spyOn(emailValidatorStub, 'isValid')
  const httpRequest = {
    body: {
      email: 'any_email@mail.com',
      password: 'any_password'
    }
  }
  await sut.handle(httpRequest)
  expect(isValidSpy).toHaveBeenCalledWith('any_email@mail.com')
})

test('Should return 400 if an invalid email is provided', async () => {
  const { sut, emailValidatorStub } = makeSut()
  jest.spyOn(emailValidatorStub, 'isValid').mockReturnValueOnce(false)
  const httpRequest = {
    body: {
      email: 'any_email@mail.com',
      password: 'any_password'
    }
  }
  const httpResponse = await sut.handle(httpRequest)
  expect(httpResponse).toEqual(badRequest(new InvalidParamError('email')))
})

test('Should return 500 if Email Validator throws', async () => {
  const { sut, emailValidatorStub } = makeSut()
  jest.spyOn(emailValidatorStub, 'isValid').mockImplementationOnce(() => {
    throw new Error()
  })
  const httpRequest = {
    body: {
      email: 'any_email@mail.com',
      password: 'any_password'
    }
  }
  const httpResponse = await sut.handle(httpRequest)
  expect(httpResponse.statusCode).toBe(500)
  expect(httpResponse).toEqual(serverError(new Error()))
})

test('Should call Authentication with correct values', async () => {
  const { sut, authenticationStub } = makeSut()
  const authSpy = jest.spyOn(authenticationStub, 'auth')
  const httpRequest = {
    body: {
      email: 'any_email@mail.com',
      password: 'any_password'
    }
  }
  await sut.handle(httpRequest)
  expect(authSpy).toHaveBeenCalledWith({
    email: 'any_email@mail.com',
    password: 'any_password'
  })
})

test('Should return 401 if invalid credentials are provided', async () => {
  const { sut, authenticationStub } = makeSut()
  jest.spyOn(authenticationStub, 'auth').mockReturnValueOnce(new Promise(resolve => resolve(null)))
  const httpRequest = {
    body: {
      email: 'any_email@mail.com',
      password: 'any_password'
    }
  }
  const httpResponse = await sut.handle(httpRequest)
  expect(httpResponse.statusCode).toBe(401)
  expect(httpResponse).toEqual(unauthorized())
})

test('Should return 500 if Authentication throws', async () => {
  const { sut, authenticationStub } = makeSut()
  jest.spyOn(authenticationStub, 'auth').mockReturnValueOnce(new Promise((resolve,reject) => reject(new Error())))
  const httpRequest = {
    body: {
      email: 'any_email@mail.com',
      password: 'any_password'
    }
  }
  const httpResponse = await sut.handle(httpRequest)
  expect(httpResponse.statusCode).toBe(500)
  expect(httpResponse).toEqual(serverError(new Error()))
})

test('Should return 200 if valid credentials are provided', async () => {
  const { sut } = makeSut()

  const httpRequest = {
    body: {
      email: 'any_email@mail.com',
      password: 'any_password'
    }
  }
  const httpResponse = await sut.handle(httpRequest)
  expect(httpResponse.statusCode).toBe(200)
  expect(httpResponse).toEqual(ok({
    accessToken: 'any_token'
  }))
})

export interface EmailValidator {
  // This is what the real EmailValidator implementation should return:
  // a boolean if an e-mail is provided
  // It seems that the interface is just a way to decouple the details and
  // further dependencies of the method. It delegates the responsibility to Validate Email
  // to the EmailValidatorAdapter
  // Still to see how it will be tied together
  isValid: (email: string) => boolean
}

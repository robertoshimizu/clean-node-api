import { AddAccount, AddAccountModel, AccountModel, Encrypter, AddAccountRepository } from './db-add-account-protocols'

export class DbAddAccount implements AddAccount {
  private readonly encrypter: Encrypter
  private readonly addAccountRepository: AddAccountRepository

  constructor (encrypter: Encrypter, addAccountRepository: AddAccountRepository) {
    this.encrypter = encrypter
    this.addAccountRepository = addAccountRepository
  }

  async addNewAccount (accountData: AddAccountModel): Promise<AccountModel> {
    // 1) Recebe o password criptografado
    const hashedPassword = await this.encrypter.encrypt(accountData.password)

    // 2) Para então assignar no campo password de um clone do AccountData
    const account = await this.addAccountRepository.addNewAccount(Object.assign({}, accountData, { password: hashedPassword }))

    // 3) Retorno do Account proveinente do AddNewAccount
    return account
  }
}

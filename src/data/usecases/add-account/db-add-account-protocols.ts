// Centralize all interfaces in a single file
// Have a single file to get the dependencies

export * from '../../../domain/usecases/add-account'
export * from '../../../domain/models/account'
export * from '../../protocols/criptography/encrypter'
export * from '../../protocols/db/add-account-repository'

import env from './config/env'
import { MongoHelper } from '../infra/db/mongodb/helpers/mongo-helper'

// Este arquivo somente roda a aplicação.
// As configurações do app do express estão na pasta config

// Deve-se conectar o Mongodb, antes de subir o Express (webserver)
MongoHelper.connect(env.mongoUrl)
  .then(async () => {
    console.log(`Mongodb connected at ${env.mongoUrl}`)
    // inicializa instancia do Node Express
    const app = (await import('./config/app')).default
    // põe o express para escutar ...
    app.listen(env.port, () => console.log(`Server running at http://localhost:${env.port}`))
  })
  .catch(console.error)

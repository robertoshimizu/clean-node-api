import { Express } from 'express'
import { bodyParser, cors, contentType } from '../middlewares/index'

// Aqui é onde eu vou adicionando os middlewares na config do app.
// Estes middlewares foram criados na pasta /main/middlewares

export default (app: Express): void => {
  app.use(bodyParser, cors, contentType)
}

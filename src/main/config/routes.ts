import { Express, Router } from 'express'
import fg from 'fast-glob'

// Aqui é onde eu vou adicionando as rotas do app
// Bilbioteca fast-glob permite que se sincronize as rotas com os
// arquivos que terminam com *routes e que ficam na pasta routes

export default (app: Express): void => {
  const router = Router()
  app.use('/api', router)
  fg.sync('**/src/main/routes/**routes.ts').map(async file => {
    const route = (await import(`../../../${file}`)).default
    route(router)
  })
}

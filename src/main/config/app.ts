import express from 'express'
import setupMiddlewares from './middlewares'
import setupRoutes from './routes'

// Aqui é onde a instancia app Express é criado e os middlewares injetados
// No final o app é injetado no server.ts para rodar
const app = express()
// Middleware tratam o que fazer com req e res
// ate agora temos: bodyParser, cors e contentType
setupMiddlewares(app)

// Seta os enderecos, e os controllers que devem ser chamados
// ate agora temos o /api/signup, que manda o req para o SignUp Controller
setupRoutes(app)
export default app

// Middleware para transformar o conteudo que vem do servidor
// para json por default

import request from 'supertest'
import app from '../config/app'

// Definiu por default que o content type deve ser passado em formato json
describe('Content Type Middleware', () => {
  test('Should return default content type as json', async () => {
    app.get('/test_content_type', (req, res) => {
      res.send('')
    })
    await request(app)
      .get('/test_content_type')
      .expect('content-type', /json/)
  })

  // Forçando a barra caso queira que o content type venha em outro formato
  // por exemplo xml

  test('Should return xml content type when forced', async () => {
    app.get('/test_content_type_xml', (req, res) => {
      res.type('xml')
      res.send('')
    })
    await request(app)
      .get('/test_content_type_xml')
      .expect('content-type', /xml/)
  })
})

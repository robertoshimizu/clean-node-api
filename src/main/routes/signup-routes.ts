import { Router } from 'express'
import { adaptRoute } from '../adapters/express-route-adapter'
import { makeSignUpController } from '../factories/signup'

export default (router: Router): void => {
  // post to express retorna um res,res
  // signupcontroller handle recebe httprequest e retorna httpresponse
  // para alinhavar é necessario criar um adapter

  router.post('/signup', adaptRoute(makeSignUpController()))
}

import { Controller, HttpRequest } from '../../presentation/protocols'
import { Request, Response } from 'express'

// este adapter recebe um objeto do tipo Controller e retorna um req e res
// no miolo ele pega um req do express, converte para HttpRequest
// manda para o controller handle, que trata, e no final ele retorna
// um http response, que o adapter converte para res, para Express
// ele desacoplou o Express da camada interior

export const adaptRoute = (controller: Controller) => {
  return async (req: Request, res: Response) => {
    const httpRequest: HttpRequest = {
      body: req.body
    }
    const httpResponse = await controller.handle(httpRequest)
    if (httpResponse.statusCode === 200) {
      res.status(httpResponse.statusCode).json(httpResponse.body)
    } else {
      res.status(httpResponse.statusCode).json({
        error: httpResponse.body.message

      })
    }
  }
}

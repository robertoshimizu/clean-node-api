// import { MongoClient } from 'mongodb'
import { MongoHelper } from '../helpers/mongo-helper'
import { AccountMongoRepository } from './account'

describe('Account MongoDb Repository - Teste de Integração', () => {
  // Tudo isso abaixo foi substituido pelo helper
  //   let client: MongoClient

  // beforeAll(async () => {
  //   client = await MongoClient.connect(process.env.MONGO_URL, {
  //     useNewUrlParser: true,
  //     useUnifiedTopology: true
  //   })
  // })
  // afterAll(async () => {
  //   await client.close
  // })

  beforeAll(async () => {
    await MongoHelper.connect(process.env.MONGO_URL)
  })
  afterAll(async () => {
    await MongoHelper.disconnect()
  })

  // zera o banco de dados antes de iniciar o teste
  beforeEach(async () => {
    const accountCollection = await MongoHelper.getCollection('accounts')
    await accountCollection.deleteMany({})
  })

  const makeSut = (): AccountMongoRepository => {
    return new AccountMongoRepository()
  }

  test('Should return an account on success', async () => {
    const sut = makeSut()
    const account = await sut.addNewAccount({
      name: 'any_name',
      email: 'any_email@mail.com',
      password: 'any_password'
    })

    // Isso porque não se sabe qual o uid que o MongoDB irá retornar
    expect(account).toBeTruthy()
    expect(account.uid).toBeTruthy()
    expect(account.name).toBe('any_name')
    expect(account.email).toBe('any_email@mail.com')
    expect(account.password).toBe('any_password')
  })
})

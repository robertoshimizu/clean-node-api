Feature: Name is provided?
    controller should return 400 if no name is provided

    Scenario: Name is not provided
        Given name is not provided
        When client send a http request
        Then server should return error 400


Feature: Email is provided?
    controller should return 400 if no Email is provided

    Scenario: Email is not provided
        Given Email is not provided
        When client send a http request
        Then server should return error 400

Feature: Password is provided?
    controller should return 400 if no Password is provided

    Scenario: Password is not provided
        Given Password is not provided
        When client send a http request
        Then server should return error 400


Feature: Password confirmation is provided?
    controller should return 400 if no Password confirmation is provided

    Scenario: Password confirmation is not provided
        Given Password confirmation is not provided
        When client send a http request
        Then server should return error 400


Feature: Password confirmation matches?
    controller should return 400 if no Password confirmation fails

    Scenario: Password confirmation does not match with password
        Given Password confirmation
        When it does not match if password
        Then server should return error 400


Feature: Is EmailValidator called with correct Email?
    controller should call EmailValidator with correct Email

    Scenario: EmailValidator is called with correct Email
        Given the email is correct
        When EmailValidator is called with
        Then EmailValidator should return isValid

Feature: Is email valid?
    controller should return 400 if email is invalid

    Scenario: Email is invalid
        Given the email is invalid
        When EmailValidator declares it to be invalid
        Then server should return error 400



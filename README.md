## Clean-Node-API project

## Typescript Development Environment

### Instalando typescript

```bash
npm install typescript@4.2.2  @types/node --save-dev
```

Set up tsconfig to config how compiler would transform a ts file to a js file.

```json
{
  "compilerOptions": {
    "outDir": "./dist",
    "module": "CommonJS",
    "target": "es2019",
    "esModuleInterop": true, // convert Common to ES
    "allowJs": true // allow js to be imported instead of .ts
  }
}
```

#### Aditional Hacks

- Git shortcuts
- Only push to git, if commit message follow Conventional Commit guidelines: [git-commit-msg-linter](https://www.npmjs.com/package/git-commit-msg-linter)
- [ESLint Standard for Typescript](https://www.npmjs.com/package/eslint-config-standard-with-typescript). Use the configuration in `.eslintrc.json`. In this file, you can define rules for the Linter to ignore. Also, in VSCode, enable plugin ESLint and disable StandardJS (to avoid conflict).
- [Husky](https://www.npmjs.com/package/husky). Husky improves your commits and more. You can use it to lint your commit messages, run tests, lint code, etc... when you commit or push. Husky supports all Git hooks.
  In our case, we want to lint only files that have been in the staged status of git (need to also install dependency [lint-staged](https://www.npmjs.com/package/lint-staged))

```json
// .huskyrc.json
{
  "hooks": {
    "pre-commit": "lint-staged",
    "pre-push": "npm run test:ci"
  }
}
```

the `lint-staged` will run according to the script below, before it commits to git repository:

```json
// .lintstagerc.json
{
  "*.ts": ["eslint 'src/**' --fix", "npm run test:staged"]
}
```

- [Jest](https://www.npmjs.com/package/jest) to perform unit tests.

```bash
npm i -D jest @types/jest ts-jest
```

Take a look in all `jest config` files. And also in the `package.json` to see all types of npm test scripts.



## Project's Architeture

This project uses [CLEAN and SOLID principles](./CLEAN.md), [TDD](./TDD.md) and [Design Patterns](./DESIGNPATTERNS.md).

1. Signup/AddAccount

![DDD](./assets/signup-diagram.png)

2. Login/Authenticate

![DDD](./assets/login-diagram.png)

`Validator` is a npm library of string validators and sanitizers.

```
import validator from 'validator'
validator.isEmail('foo@bar.com'); //=> true
```

### Mongodb

For unit testing, we have used `mongodb` library, which simulates a mongodb CRUD transaction in memory. However, when doing integration testing, especially when checking the composition of the various dependencies, it is necessary to connect to a mongodb instance.

There are 2 implementations of mongodb in docker:

1. Node native in your machine. Just spin docker-compose in the project root. You can inspect it using Compass (Mongodb IDE)

2. Node in a vscode .devcontainer. For that we have deployed a mongodb in a docker container, in addition to the node. They are both in a docker-compose, inside the `.devcontainer.json`. We have added the `forward ports: [27017]`, so we could inspect what is going on by using Postman or Compass (Mongodb IDE). Do not forget to connect the `mongodb app`. Use the VSCode extension.

### Starting the application

To start use:

```bash
npm start
```

Express Server will start and you should see the following message:

```bash
Mongodb connected at mongodb://localhost:27017/clean-node-api
Server running at http://localhost:5050
```

To test if it is working, make a POST request using `postman`:

```postman
http://localhost:5050/api/signup
```

and fill out the request`s body with:

```json
{
  "name": "valid_name",
  "email": "valid@email.com",
  "password": "valid_password",
  "passwordConfirmation": "valid_password"
}
```

Then you should visualize the server response with the newly created account.

### References:

- [Git - Pretty Formats](https://git-scm.com/docs/pretty-formats)
- [Git - Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)
- [Understand Async/Await in TypeScript](https://levelup.gitconnected.com/understand-async-await-in-typescript-in-only-a-few-minutes-dedb5a18a2c)
- [Typescript - Interfaces](https://www.tutorialsteacher.com/typescript/typescript-interface)
- [Validator.js](https://www.npmjs.com/package/validator)
- [Bcrypt](https://github.com/kelektiv/node.bcrypt.js#readme)
- [jest-mongodb](https://github.com/shelfio/jest-mongodb)
- [TDD](./TDD.md)

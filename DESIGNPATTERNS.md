# Design Patterns used in this project

## Decorator Pattern

When you want to modify the behavior of a class withou modifying it - Open Closed Principle, you can make another class of the same signature, and wrap it around the original class.

Notice below, that we instatiate `signUpController`, and then we inject it in a new class called `LogSignUpControllerDecorator`. Both classes implement the interface `Controller`, and therefore they both have the same signature.

```Typescript
xport const makeSignUpController = (): Controller => {
  const salt = 12
  const emailValidatorAdapter = new EmailValidatorAdapter()
  const bcryptAdapter = new BcryptAdapter(salt)
  const accountMongoRepository = new AccountMongoRepository()
  const dbAddAccount = new DbAddAccount(bcryptAdapter, accountMongoRepository)
  const signUpController = new SignUpController(emailValidatorAdapter, dbAddAccount)
  const logSignUpControllerDecorator = new LogSignUpControllerDecorator(signUpController)
  return logSignUpControllerDecorator
}

```

Now, in the class `LogSignUpControllerDecorator` we call the same method of the original class (i.e. `handle`), but now we can modify its behavior by adding more code, and then at the end we return the new result.

```Typescript
class LogSignUpControllerDecorator implements Controller{
  private readonly controller: Controller

  constructor(controller: Controller){
    this.controller = controller
  }
  async handle (httpRequest: HttpRequest): Promise <HttpResponse> {
    const httpResponse = await this.controller.handle(httpRequest)

    // block of code that we could add to alter the result of the original httpResponse

    const modifiedHttpResponse = httpResponse

    return modifiedHttpResponse
  }

}

```

Return to [Clean-Node-API project](./README.md).

## Domain Driven Design and SOLID Principles

This is a project following the Udemy course [NodeJs, Typescript, TDD, DDD, Clean Architecture e SOLID](https://www.udemy.com/course/tdd-com-mango/) from [Rodrigo Manguinho](https://www.linkedin.com/in/rmanguinho).

Some lessons learned:

- Before start coding, make sure you have all customer and system requirements, class diagrams and architecture already defined. Otherwise, you will struggle to decide what to do on the fly.
- Especially for TDD, it is advisable that you know upfront what classes and its methods to test, as well as what would be the desired outcome. There is a specific section covering the main learnings from [TDD](./TDD.md).

The project's architecture uses both Domain Driven Design (DDD) and CLEAN architectures (Uncle Bob). This approach preaches designing software by using a layered architecture with a domain model at the center, which deal with business logic, and interfaces at the periphery, which deals with data and user.

![domain](./assets/domain-layer.svg)
![CLEAN](./assets/CleanArchitecture.jpeg)
The goal of these two architectures is to produce systems that are:

- **Independent of Frameworks**. The architecture does not depend on the existence of some library of feature laden software. This allows you to use such frameworks as tools, rather than having to cram your system into their limited constraints.
- **Testable**. The business rules can be tested without the UI, Database, Web Server, or any other external element.
- **Independent of UI**. The UI can change easily, without changing the rest of the system. A Web UI could be replaced with a console UI, for example, without changing the business rules.
- **Independent of Database**. You can swap out Oracle or SQL Server, for Mongo, BigTable, CouchDB, or something else. Your business rules are not bound to the database.
- **Independent of any external agency**. In fact your business rules simply don’t know anything at all about the outside world.

The concentric circles represent different areas of software. In general, the further in you go, the higher level the software becomes. The outer circles are mechanisms. The inner circles are policies.

**The overriding rule that makes this architecture work is The Dependency Rule**. This rule says that source code dependencies can only point inwards. Nothing in an inner circle can know anything at all about something in an outer circle. In particular, the name of something declared in an outer circle must not be mentioned by the code in the an inner circle. That includes, functions, classes. variables, or any other named software entity.

By the same token, data formats used in an outer circle should not be used by an inner circle, especially if those formats are generate by a framework in an outer circle. We don’t want anything in an outer circle to impact the inner circles.

More on this can be found in this [article](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)

Return to [Clean-Node-API project](./README.md).